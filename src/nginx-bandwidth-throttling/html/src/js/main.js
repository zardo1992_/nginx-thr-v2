$(document).ready(function() {

    var stringLog = "";
    var validator;
    var confirmButton;

   
    stringLog += " proxy_pass set to http://vod.pcdn.any.sky.it/\n";
    stringLog += " Download sample: http://35.205.84.208/filter/RM/live/HD/skycinema/MOVIE/skycinema-MVXX0000000001223602-201803271645500000.nff\n\n"; 

    $("textarea").text(stringLog);

    validator = $("#form").bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            ipaddr: {
                validators: {
                    notEmpty: {
                        message: 'Please, supply the IP address you want to throttle.'
                    }
                }
            },
            bps: {
                validators: {
                    notEmpty: {
                        message: 'Please, supply the throttling BPS value.'
                    }
                }
            }
        }
    });

    confirmButton = $("button").click(function() {
        $('#form').bootstrapValidator('validate');

        if(!!$('#ipaddr').val() 
                && !!$('#bps').val()) {

		    stringLog += " Waiting for server response...\n";
                    $("textarea").text(stringLog);

                    $("#container").css({
                        opacity: 0.3
                    });

                    $("#loader").css({
                        display: "block"
                    });
        
            $.ajax({
                type: 'POST',
                url: "http://35.205.84.208/limit/config",
                data: "ipaddr=" + $('#ipaddr').val() + "&bps=" + $('#bps').val(),
                success: function(data) {
		    stringLog += " " + data + "\n";

                    $("textarea").text(stringLog);

                    $("#container").css({
                        opacity: 1
                    });

                    $("#loader").css({
                        display: "none"
                    });
                },
                error: function(xhr, textStatus, errorThrown) {

		    stringLog += " " + xhr.responseText + "\n";
                    $("textarea").text(stringLog);

                    $("#container").css({
                        opacity: 1
                    });

                    $("#loader").css({
                        display: "none"
                    });
                }
            });
        }

    });
});
