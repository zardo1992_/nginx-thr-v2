# nginx-bandwidth-throttling
Since nginx permits only to limit the http band per request or per number of requests/ip, the aim of this POC is to enable nginx to shape the band by considering two parameters, which are the ip address and the bps rate value. Our solution relies on a personalization of the [Nginx-limit-traffic-rate-module](https://github.com/bigplum/Nginx-limit-traffic-rate-module), which permits to limit the rate of download requests.
## Getting Started

Follow these instructions in order to build the POC on your local machine.

### Prerequisites

* the `gcc` compiler ^(4 or higher).

### Installing

1. Clone the repository on your local machine.
2. Download `nginx` ^(1.12.2 or higher) from the [nginx website](http://nginx.org/en/download.html) and uncompress the archive.
3. Move to the uncompressed archive.
    ```
    cd nginx-1.12.2/
    ```
4. Configure `nginx` by adding the modules provided in this git repository within the `modules` folder. 
    ```
    ./configure 
        --add-module=..path/to/modules/lua-nginx-module-0.10.12rc2             
        --add-module=..path/to/modules/Nginx-limit-traffic-rate-module.master
        --add-module=..path/to/modules/ngx_devel_kit-0.3.1rc1
        --prefix=/opt/nginx 
        --with-debug
    ```
    This command will install `nginx` at the path `/opt/nginx`. If you want to change your installation path, you should change the `--prefix` value. You should set `..path/to/modules/` properly by specifying where the modules are.

5. Compile and install `nginx` by using this command.
    ```
    make && make install
    ```
    The `nginx` instance should be available at `/opt/nginx`.
    
6. Overwrite the `nginx.conf` configuration file by copying the one provided in this git repository.
    ```
    cp nginx.conf /opt/nginx/conf/
        > overwrite? Y
    ```

7. Define a new folder named `addresses` in `/opt/nginx/conf`.
    ```
    mkdir /opt/nginx/conf/addresses
    ```
8. Now you can start the `nginx` server instance.
    ```
    /opt/nginx/sbin/nginx
    /opt/nginx/sbin/nginx -s reload
    ```
## Authors

* **Matteo Zardoni** - [ma.zardoni@reply.it](mailto:ma.zardoni@reply.it)


