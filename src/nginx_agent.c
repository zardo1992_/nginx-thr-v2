/*
	===============================================================================
	 nginx-throttling: C AGENT
	===============================================================================

	A C program to be executed in background. Its scope is to subscribe itself to
	a Redis environment in an async manner, in order to notify nginx, given an
	user_id or an IP address, the speed that it has to apply in order to perform 
	the throttling activity properly.

												Matteo Zardoni, ma.zardoni@reply.it
	===============================================================================
*/

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <hiredis/async.h>
#include <hiredis/hiredis.h>
#include <hiredis/adapters/libevent.h>

// Static variables
static char *dat_files_path = "/opt/nginx/conf/addresses"; //WITHOUT final slash

// Function prototypes
void onMessage(redisAsyncContext *c, void *reply, void *privdata);

int main(int argc, char **argv)
{
	signal(SIGPIPE, SIG_IGN);

	// Variables
	char *hostname = getenv("REDIS_IPADDR");
	int port = atoi(getenv("REDIS_PORT"));
	struct event_base *base;
	int status;
	redisAsyncContext *c;
	char msg[30];

	base = event_base_new();
	c = redisAsyncConnect(hostname, port);

	if (!c || c->err)
	{
		printf("Connection error: %s\n", c->errstr);
		return 1;
	}

	redisLibeventAttach(c, base);
	printf("Connected to REDIS!\n");
	strcpy(msg, "psubscribe *.*.*.*");
	status = redisAsyncCommand(c, onMessage, (char *)"sub", msg);
	if (status != REDIS_OK)
	{
		printf("Command exec error! (status = %d)\n", status);
	}
	event_base_dispatch(base);
	return 0;
}

void onMessage(redisAsyncContext *c, void *reply, void *privdata)
{
	redisReply *r = reply;
	FILE *f;
	char file_path[100];

	if (!r)
	{
		printf("An empty response has arrived!");
	}

	if (r->type == REDIS_REPLY_ARRAY && r->elements == 4)
	{
		char *msg = r->element[3]->str;
		char bps[100];
		char user_id[100];

		if (msg)
		{
			char *token = strtok(msg, ",");
			int counter = 0;

			while (token != NULL)
			{
				switch (counter)
				{
				case 0:
					strcpy(bps, token);
					break;

				case 1:
					strcpy(user_id, token);
					break;

				default:
					return;
					break;
				}
				token = strtok(NULL, ",");
				counter++;
			}

			if (counter < 2)
			{
				// NO user_id
				strcpy(file_path, dat_files_path);
				strcat(file_path, "/");
				strcat(file_path, r->element[2]->str);
				strcat(file_path, ".DAT");
				if (access(file_path, F_OK) != -1)
				{
					f = fopen(file_path, "w");
					fprintf(f, "%s", bps);
					fclose(f);
					setbuf(stdout, NULL);
					printf("\t%s >>> %s \t has been stored.\n", file_path, bps);
				}
				else
				{
					printf("\t%s >>> %s \t skipped.\n", file_path, bps);
				}
			}
			else
			{
				//A user_id has been forwarded
				strcpy(file_path, dat_files_path);
				strcat(file_path, "/");
				strcat(file_path, user_id);
				strcat(file_path, ".DAT");
				if (access(file_path, F_OK) != -1)
				{
					f = fopen(file_path, "w");
					fprintf(f, "%s;%s", r->element[2]->str, bps);
					fclose(f);
					setbuf(stdout, NULL);
					printf("\t%s >>> %s;%s \t has been stored.\n", file_path, r->element[2]->str, bps);
				}
				else
				{
					printf("\t%s >>> %s;%s \t skipped.\n", file_path, r->element[2]->str, bps);
				}
			}
		}
	}

	return;
}
