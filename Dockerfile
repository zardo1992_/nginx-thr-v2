# ##############################################################################
# Definition of the image
# >>> docker-compose build
# #############################################################################
# Importing Debian environment
FROM debian:latest
# Copying nginx-throttling source files
COPY src/nginx-1.12.2 /etc/nginx-1.12.2/
COPY src/nginx-bandwidth-throttling /etc/nginx-bandwidth-throttling
COPY src/nginx_agent.c /etc
COPY src/start.sh /usr/local/bin/
COPY src/nginx /etc/logrotate.d/
# Installing nginx with all its dependencies
RUN	echo " ############ Installing dependencies..." \
	&& apt-get -y update \
	&& apt-get -y install build-essential \
	&& apt-get -y install libpcre3-dev \
	&& apt-get -y install zlib1g-dev \
	&& apt-get -y install libhiredis-dev \
	&& apt-get -y install libevent-dev \
	&& echo " ############ Installing nginx-thr app..." \
	&& cd /etc/nginx-1.12.2/ \
	&& ./configure --add-module=/etc/nginx-bandwidth-throttling/modules/Nginx-limit-traffic-rate-module.master --prefix=/opt/nginx --with-debug --with-ld-opt="-L /usr/local/lib -lhiredis" \
	&& make && make install \
	&& cp /etc/nginx-bandwidth-throttling/nginx.conf /opt/nginx/conf/ \
	&& mkdir /opt/nginx/conf/addresses \
	&& echo " ############ Finishing configuration..." \ 
	&& chmod 777 /opt/nginx/conf/addresses \
	&& echo " ############ Compiling agent..." \
	&& cd /etc && gcc nginx_agent.c -lhiredis -levent -o agent \
	&& cp agent /opt/nginx/sbin/ \
	&& echo " ############ Installation finished!"
# Exposing port properly
EXPOSE 80 443
# Starting nginx
CMD /usr/local/bin/start.sh ; sleep infinity